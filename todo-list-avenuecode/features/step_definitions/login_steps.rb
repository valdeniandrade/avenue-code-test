Given("User is on Sign in screen") do
    visit '/sign_in'
  end
  
When("do a login with {string} and {string}") do |email, password|
    login_page = LoginPage.new
    login_page.access
    login_page.login(email, password)
  end
  
  Then("see the following message {string}") do |message|
    expect(page).to have_content message
  end