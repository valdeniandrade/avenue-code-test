Given("User is logged with {string} and {string}") do |email, password|
    visit '/sign_in'
    login_page = LoginPage.new
    login_page.access
    login_page.login(email, password)
    sleep 5
end

When("Access My Taks screen") do
    find('.btn').click
end

Then("create a new task with {string}") do |task_name|
find('input[name=new_task]').set task_name
sleep 5
find('.input-group-addon').click
end