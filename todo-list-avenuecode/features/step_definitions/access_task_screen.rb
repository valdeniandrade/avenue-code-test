Given("User is authenticated with {string} and {string}") do |email, password|
  visit '/sign_in'
  login_page = LoginPage.new
  login_page.access
  login_page.login(email, password)
  sleep 10
end

When("click on Tasks button") do
  find('.btn').click
end

Then("see the title {string}") do |title|
  expect(page).to have_content title
end