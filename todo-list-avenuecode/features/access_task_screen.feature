@task2

Feature: Task Screen

  Scenario: User should be able to access New Tasks screen
  
    Given User is authenticated with "valdeniandrade@gmail.com" and "teste@123"
    When click on Tasks button
    Then see the title "Valdenia's ToDo List"