require 'capybara'
require 'capybara/cucumber'

Capybara.configure do |config|
    config.default_driver = :selenium
    config.app_host = 'https://qa-test.avenuecode.com/users'
end