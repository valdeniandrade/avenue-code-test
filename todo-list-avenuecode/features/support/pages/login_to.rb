class LoginPage
    include Capybara::DSL
    def access
        visit '/sign_in'
    end
    def login(email, password)
    find('#user_email').set email
    find('#user_password').set password
    find('input[name=commit]').click
    end
end